package com.blsm.appdataModel;

public class ProductCount {
	private String id;
	private int buycount;
	private int pv; 
	private String _name;
	private String _url;
	
	 public String get_name() {
		return _name;
	}

	public void set_name(String _name) {
		this._name = _name;
	}

	public String get_url() {
		return _url;
	}

	public void set_url(String _url) {
		this._url = _url;
	}

	public String getDataTime() {
		return dataTime;
	}

	public void setDataTime(String dataTime) {
		this.dataTime = dataTime;
	}

	public String getImpTime() {
		return impTime;
	}

	public void setImpTime(String impTime) {
		this.impTime = impTime;
	}

	String dataTime="";
	    String impTime="";

	public ProductCount(String id, int buycount, int pv) {
		super();
		this.id = id;
		this.buycount = buycount;
		this.pv = pv;
	}

	public ProductCount() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getBuycount() {
		return buycount;
	}

	public void setBuycount(int buycount) {
		this.buycount = buycount;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}
	
}

