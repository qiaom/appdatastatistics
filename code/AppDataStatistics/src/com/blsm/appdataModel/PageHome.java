package com.blsm.appdataModel;

public class PageHome {
	int id=0;
	int pv=0;
	int d_id=0;
	int num=0;
	String des="";
	String _name="";
	String _url="";
    String dataTime="";
    String impTime="";
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPv() {
		return pv;
	}
	public void setPv(int pv) {
		this.pv = pv;
	}
	public String get_name() {
		return _name;
	}
	public void set_name(String _name) {
		this._name = _name;
	}
	public String get_url() {
		return _url;
	}
	public void set_url(String _url) {
		this._url = _url;
	}

	public int getid() {
		return id;
	}
	public void set_id(int id) {
		this.id = id;
	}
	public int get_pv() {
		return pv;
	}
	public void set_pv(int pv) {
		this.pv = pv;
	}
	public int getD_id() {
		return d_id;
	}
	public void setD_id(int d_id) {
		this.d_id = d_id;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getDataTime() {
		return dataTime;
	}
	public void setDataTime(String dataTime) {
		this.dataTime = dataTime;
	}
	public String getImpTime() {
		return impTime;
	}
	public void setImpTime(String impTime) {
		this.impTime = impTime;
	}
	public PageHome(int id, int pv, int d_id, int num, String des,
			String dataTime, String impTime) {
		super();
		this.id = id;
		this.pv = pv;
		this.d_id = d_id;
		this.num = num;
		this.des = des;
		this.dataTime = dataTime;
		this.impTime = impTime;
	}
	public PageHome() {
		super();
	}
	

}
