package com.blsm.appdataModel;

public class ShopCount {
	int carts;
	int orders;
	int activity;
	int new_add;
	int collect;
	
	 String dataTime="";
	    String impTime="";
	    
	public int getCarts() {
		return carts;
	}
	public void setCarts(int carts) {
		this.carts = carts;
	}
	public int getOrders() {
		return orders;
	}
	public void setOrders(int orders) {
		this.orders = orders;
	}
	public int getActivity() {
		return activity;
	}
	public void setActivity(int activity) {
		this.activity = activity;
	}
	public int getNew_add() {
		return new_add;
	}
	public void setNew_add(int new_add) {
		this.new_add = new_add;
	}
	public int getCollect() {
		return collect;
	}
	public void setCollect(int collect) {
		this.collect = collect;
	}
	public ShopCount(int carts, int orders, int activity, int new_add,
			int collect) {
		super();
		this.carts = carts;
		this.orders = orders;
		this.activity = activity;
		this.new_add = new_add;
		this.collect = collect;
	}
	public ShopCount() {
		super();
	}

}
