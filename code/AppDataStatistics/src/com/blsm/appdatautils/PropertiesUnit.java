package com.blsm.appdatautils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

/**
 * Msg filter
 * 
 * @since 2013-12-21
 * @author qiaoming
 * 
 */

public class PropertiesUnit {

	// 配置文件中的数据
	public static final int C_COUNT = 0;
	public static final int C_CONTENT = 1;

	// 配置文件路径
	public static final String FILE_NAME = "config/urlconfig.properties";

	public Properties properties;
	public FileInputStream fis;
 

	public PropertiesUnit() {
		super();

		File file = new File(FILE_NAME);
		try {
			fis = new FileInputStream(file);
			properties = new Properties();
			properties.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 在配置文件中保存配置信息
	 * 
	 * @param key
	 * @param value
	 */
	public void setFilter(String key, String value) {
		
		OutputStream fos;
		try {
			fos = new FileOutputStream(FILE_NAME);
			properties.setProperty(key.trim(), value.trim());
			properties.store(fos, null);
			fis.close();
			fos.flush();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public String getFilter(String s) {
		 
		String value=properties.getProperty(s);
		try {
			fis.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return value;
		
	}
	
	
	
	 

	/**
	 * 删除配置文件中关键字为key的配�?
	 * 
	 * @param key
	 */
	public void delete(String key) {

		try {
			OutputStream fos = new FileOutputStream(FILE_NAME);
			properties.remove(key);
			properties.store(fos, null);
			fis.close();
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {

		PropertiesUnit mUnit = new PropertiesUnit();
		mUnit.setFilter("product", "10,view://123");
		mUnit.setFilter("article", "20,view://123");
		mUnit.setFilter("order", "30,view://123");
 
		mUnit.delete("product");
	}
}
