package com.blsm.appdatautils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class AppendToFile {

	public static final String fileName = "output/output.txt";
    public static List<String[]> fileList=new ArrayList<String[]>();
	public static void appendMethodA(String content) {
		try {

			RandomAccessFile randomFile = new RandomAccessFile(fileName, "w");

			long fileLength = randomFile.length();

			randomFile.seek(fileLength);
			randomFile.writeUTF(content);
			randomFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
//	
//	读取
//
//    import java.io.BufferedReader;  
//    import java.io.FileInputStream;  
//    import java.io.InputStreamReader;  
//
//    String FileContent = ""; // 文件很长的话建议使用StringBuffer 
//    try { 
//        FileInputStream fis = new FileInputStream("d:\\input.txt"); 
//        InputStreamReader isr = new InputStreamReader(fis, "UTF-8"); 
//        BufferedReader br = new BufferedReader(isr); 
//        String line = null; 
//        while ((line = br.readLine()) != null) { 
//            FileContent += line; 
//            FileContent += "\r\n"; // 补上换行�?
//        } 
//    } catch (Exception e) { 
//        e.printStackTrace(); 
//    }
//
//写入
//
//    import java.io.FileOutputStream; 
//    import java.io.OutputStreamWriter; 
//
//    String FileContent = "文件内容"; 
//    try { 
//        FileOutputStream fos = new FileOutputStream("d:\\output.txt"); 
//        OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8"); 
//        osw.write(FileContent); 
//        osw.flush(); 
//    } catch (Exception e) { 
//        e.printStackTrace(); 
//    }
//	
	
	
	

	public static void readOutFile(String content,String name) {
		try {
			String fname="data/"+name+".txt";
			File file=new File(fname);
			if(!file.exists()){
			file.createNewFile();
			}
			FileWriter writer = new FileWriter(file, true);
			writer.write(content);
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

 
	
	public boolean Fdel(String name){
        File file=new File("data/"+name+".txt");
		return file.delete();
		 
	}
	public static void main(String[] args) {
	 
	}

}
