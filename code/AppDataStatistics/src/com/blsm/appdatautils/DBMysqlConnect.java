package com.blsm.appdatautils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
 

public class DBMysqlConnect {
 	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static String URL = "jdbc:mysql://localhost:3306/my_db";
	private static String USER_NAME = "root";
	private static String PASSWORD = "root";
	private static DBMysqlConnect mySQLConnect;

 

	public static DBMysqlConnect getInstance() {
		if (mySQLConnect == null) {
			mySQLConnect = new DBMysqlConnect();
		}
		return mySQLConnect;
	}

	public Connection getDBConnection() {
		try {
			Class.forName(DRIVER);

			Connection connection = DriverManager.getConnection(URL + "?"
					+ "user=" + USER_NAME + "&password=" + PASSWORD
					+ "&useUnicode=true&characterEncoding=utf8");
			//connection.setAutoCommit(true);
			return connection;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}

 