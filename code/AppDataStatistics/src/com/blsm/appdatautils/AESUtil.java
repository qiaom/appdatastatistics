package com.blsm.appdatautils;

import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtil {

	public static byte[] joinBytes(byte[] byte_1, byte[] byte_2) {
		byte[] data = new byte[byte_1.length + byte_2.length];
		System.arraycopy(byte_1, 0, data, 0, byte_1.length);
		System.arraycopy(byte_2, 0, data, byte_1.length, byte_2.length);
		return data;
	}

	public static String encrypt(String src, String encryptionKey, String iv) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, makeKey(encryptionKey), makeIv(iv));
			byte[] byte_1 = cipher.update(src.getBytes("UTF-8"));
			if (byte_1 == null) {
				byte_1 = new byte[0];
			}
			byte[] byte_2 = cipher.doFinal();
			byte[] data = AESUtil.joinBytes(byte_1, byte_2);
			return unpack(data);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String decrypt(String src, String encryptionKey, String iv) {
		String decrypted = "";
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, makeKey(encryptionKey), makeIv(iv));
			byte[] byte_1 = cipher.update(AESUtil.pack(src));
			if (byte_1 == null) {
				byte_1 = new byte[0];
			}
			byte[] byte_2 = cipher.doFinal();
			byte[] data = AESUtil.joinBytes(byte_1, byte_2);
			decrypted = new String(data, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decrypted;
	}

	public static AlgorithmParameterSpec makeIv(String iv) {
		return new IvParameterSpec(pack(iv));
	}

	public static Key makeKey(String encryptionKey) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] key = md.digest(encryptionKey.getBytes());
			return new SecretKeySpec(key, "AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String unpack(byte[] data) {

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			String hex = Integer.toHexString(data[i] & 0xff);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex);
		}
		return sb.toString();
	}

	public static byte[] pack(String s) {

		byte[] baKeyword = new byte[s.length() / 2];
		for (int i = 0; i < baKeyword.length; i++) {
			try {
				baKeyword[i] = (byte) (0xff & Integer.parseInt(
						s.substring(i * 2, i * 2 + 2), 16));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return baKeyword;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String src = "3213";
		String article_key = "AJ03lQmVmtomCfug";
		String iv = "1e5673b2572af26a8364a50af84c7d2a";
		String encrypted = AESUtil.encrypt(src, article_key, iv);
		String decrypted = AESUtil.decrypt(encrypted, article_key, iv);
		System.out.println("src: " + src);
		System.out.println("encrypted: " + encrypted);
		System.out.println("decrypted: " + decrypted);
	}

}
