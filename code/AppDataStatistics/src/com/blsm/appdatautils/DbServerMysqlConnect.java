package com.blsm.appdatautils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
 

public class DbServerMysqlConnect {
 	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static String URL = "jdbc:mysql://10.200.118.38:3306/adultshop_production";
	private static String USER_NAME = "log_analysis";
	private static String PASSWORD = "blue_1207";
	private static DbServerMysqlConnect mySQLConnect;

 

	public static DbServerMysqlConnect getInstance() {
		if (mySQLConnect == null) {
			mySQLConnect = new DbServerMysqlConnect();
		}
		return mySQLConnect;
	}

	public Connection getDBConnection() {
		try {
			Class.forName(DRIVER);

			Connection connection = DriverManager.getConnection(URL + "?"
					+ "user=" + USER_NAME + "&password=" + PASSWORD
					+ "&useUnicode=true&characterEncoding=utf8");
			//connection.setAutoCommit(true);
			return connection;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}

 