package com.blsm.appdatautils;

 

public class IdUtils {

	private static final String ARTICLE_KEY = "AJ03lQmVmtomCfug";
	private static final String PRODUCT_KEY = "XRbLEgrUCLHh94qG";
	private static final String IV = "1e5673b2572af26a8364a50af84c7d2a";

	public static String encryptArticleIdFromJNI(String id) {
		return AESUtil.encrypt(id, ARTICLE_KEY, IV);
	}

	/**
	 * Decrypt article id from JNI
	 * 
	 * @param id
	 *            An {@link Article} id
	 * @return The decrypt article id
	 */
	public static String decryptArticleIdFromJNI(String id) {
		return AESUtil.decrypt(id, ARTICLE_KEY, IV);
	}

	/**
	 * Encrypt product id from JNI
	 * 
	 * @param id
	 *            A {@link Product} id
	 * @return The encrypt product id
	 */
	public static String encryptProductIdFromJNI(String id) {
		return AESUtil.encrypt(id, PRODUCT_KEY, IV);
	}

	/**
	 * Decrypt product id from JNI
	 * 
	 * @param id
	 *            A {@link Product} id
	 * @return The decrypt product id
	 */
	public static String decryptProductIdFromJNI(String id) {
		return AESUtil.decrypt(id, PRODUCT_KEY, IV);
	}
	// TODO:
//	static {
//		System.loadLibrary("blsmid_v_1_0");
//	}
//
//	/**
//	 * Encrypt article id from JNI
//	 * 
//	 * @param id
//	 *            An {@link Article} id
//	 * @return The encrypt article id
//	 */
//	public static native String encryptArticleIdFromJNI(String id);
//
//	/**
//	 * Decrypt article id from JNI
//	 * 
//	 * @param id
//	 *            An {@link Article} id
//	 * @return The decrypt article id
//	 */
//	public static native String decryptArticleIdFromJNI(String id);
//
//	/**
//	 * Encrypt product id from JNI
//	 * 
//	 * @param id
//	 *            A {@link Product} id
//	 * @return The encrypt product id
//	 */
//	public static native String encryptProductIdFromJNI(String id);
//
//	/**
//	 * Decrypt product id from JNI
//	 * 
//	 * @param id
//	 *            A {@link Product} id
//	 * @return The decrypt product id
//	 */
//	public static native String decryptProductIdFromJNI(String id);
}
