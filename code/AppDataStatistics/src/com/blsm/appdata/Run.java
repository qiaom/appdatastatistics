package com.blsm.appdata;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.blsm.appdataModel.PageHome;
import com.blsm.appdataModel.ProductCount;
import com.blsm.appdataModel.ShopCount;
import com.blsm.appdatautils.AppendToFile;
import com.blsm.appdatautils.DBMysqlConnect;
import com.blsm.appdatautils.DbServerMysqlConnect;
import com.blsm.appdatautils.HttpUtils;
import com.blsm.appdatautils.IdUtils;
import com.blsm.appdatautils.PropertiesUnit;

public class Run {
	static String it = "";
	static PropertiesUnit pUnit = new PropertiesUnit();
	static String PATH = "";
	static Map<String, ProductCount> pro_dbdata = new HashMap<String, ProductCount>();
	/*
	 * 首页商品到购买成功数据
	 */
	static  Map<String, ProductCount> pageProductsToorder = new HashMap<String, ProductCount>();
	static List<PageHome> pagedata = new ArrayList<PageHome>();
	/*
	 * 每個商品的pv
	 */

	static HashMap<String, ProductCount> k_v_productCountData = new HashMap<String, ProductCount>();
	/*
	 * 首页到产品详情pv
	 */
	static String apppageproductsurl_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=600&";
	static String apppageproductsurl_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536de02fe8af9ceaa72ba3a0&stats=count_distribute";

	/*
	 * 首页到产品详情pv到购买成功 (需要连接服务器数据库)
	 */
  
	/*
	 * 首页分类pv
	 */
	static String apppageclassurl_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=30&";
	static String apppageclassurl_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536de182e8af9ceaa72ba3ad&stats=count_distribute";

	/*
	 * 首页活动pv
	 */
	static String apppagecamurl_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=30&";
	static String apppagecamurl_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536ddf9ae8af9ceaa72ba39d&stats=count_distribute";
	/*
	 * 首页品牌pv
	 */
	static String apppagebrandurl_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=30&";
	static String apppagebrandurl_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536de6a2e8af9ceaa72ba3c9&stats=count_distribute";

	 

	/**
	 * 新增 活跃用户
	 */
	static String appusertodayurl = "http://www.umeng.com/apps/8a6100022fb042652b42f535/reports/load_table_data?page=1&per_page=30&stats=index_today";

	/**
	 * 收藏数量
	 */
	static String appcollectrul_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=1&";
	static String appcollectrul_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&stats=event_group_trend&event_group_id=536ddf5656240b0a730b190e";
	/*
	 * 订单数量（查询后台数据库）
	 */
	static int orders = 0;
	/*
	 * 购物车数量（查询后台数据库）
	 */
	static int carts = 0;
	/*** 所有商品PV *******************************/

 	// 1.商品列表到详情
	static String product_pv_url_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=600&";
	static String product_pv_url_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536de182e8af9ceaa72ba3af&stats=count_distribute";

	// 2.首页到详情
	static String product_pv_url_3 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=600&";
	static String product_pv_url_4 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536de02fe8af9ceaa72ba3a0&stats=count_distribute";

	// 3 收藏到详情
	static String product_pv_url_5 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=600&";
	static String product_pv_url_6 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=537026cce8af9ceaa72ba665&stats=count_distribute";

	// 4.推送到详情
	static String product_pv_url_7 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=600&";
	static String product_pv_url_8 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536dee89e8af9ceaa72ba3df&stats=count_distribute";

	
	//
	static String new_todayCount_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/reports/load_table_data?page=1&per_page=1&";
	static String new_todayCount_2 = "&stats=index_details";

	static ShopCount shopCount = new ShopCount();
	static String check = "";

	// 活动专区 宝典收到次数
	static String article_url_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=30&";
	static String article_url_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=53e9cbc1e8af9ceaa72dc840&stats=count_distribute";
    

	
	// 推送进入宝典次数
	static String article_click_url_1 = "http://www.umeng.com/apps/8a6100022fb042652b42f535/events/load_table_data?page=1&per_page=300&";
	static String article_click_url_2 = "&channels%5B%5D=&versions%5B%5D=&segments%5B%5D=&event_id=536dee30e8af9ceaa72ba3de&stats=count_distribute";
    static HashMap<Integer, Integer> a_clickedsHashMap=new HashMap<Integer, Integer>();
	//推送的宝典到产品详细
	
	
	
	public static void main(String[] args) {
		if (args.length > 0) {
			check = args[0];

		}
		if ("-d".equals(check)) {
			delAll();
			System.exit(0);
		}
		 getPushCount();
        	//getArticleClickCount();
		System.exit(0);
		PATH = getYdate();
		delRepeat();
		GetServerDbInfo();
		ReadF();

		setUserInfo();
		addUserCountinfo();
		//
		setPageHome();
		AddPageHomeinfo();
		//
		setProducts();
		AddProductsinfo();
	}

	static void setPageHome() {
		getApppageclassurl();
		getApppagebrand();
		getApppagecamurl();
		getApppageproducturl();

	}

	static void setProducts() {
		getProductsJson(product_pv_url_1, product_pv_url_2);
		getProductsJson(product_pv_url_3, product_pv_url_4);
		getProductsJson(product_pv_url_5, product_pv_url_6);
		getProductsJson(product_pv_url_7, product_pv_url_8);

	}

	static void setUserInfo() {
		getAppcollect();
		getusercount();
	}

	static String getappusertoday() {
		String json = HttpUtils.APP(appusertodayurl);
		System.out.println("appusertodayurl" + json);

		jsonToArry(json, "appusertodayurl");

		return json;
	}

	static String getusercount() {
		String apppageclassurl = new_todayCount_1 + "start_date=" + PATH
				+ "&end_date=" + PATH + new_todayCount_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("appusertodayurl:" + json);
		jsonToArry(json, "appusertodayurl");
		return json;
	}

	public static String getApppageclassurl() {
		String apppageclassurl = apppageclassurl_1 + "start_date=" + getYdate()
				+ "&end_date=" + getYdate() + apppageclassurl_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("apppageclassurl:" + json);
		jsonToArry(json, "apppageclassurl");
		return json;
	}

	static String getApppageproducturl() {
		String apppageclassurl = apppageproductsurl_1 + "start_date="
				+ getYdate() + "&end_date=" + getYdate() + apppageproductsurl_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("apppageproductsurl:" + json);
		jsonToArry(json, "apppageproductsurl");
		return json;
	}

	static String getApppagecamurl() {
		String apppageclassurl = apppagecamurl_1 + "start_date=" + getYdate()
				+ "&end_date=" + getYdate() + apppagecamurl_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("apppageproductsurl:" + json);
		jsonToArry(json, "apppagecamurl");
		return json;

	}

	static String getApppagebrand() {
		String apppageclassurl = apppagebrandurl_1 + "start_date=" + getYdate()
				+ "&end_date=" + getYdate() + apppagebrandurl_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("apppageproductsurl:" + json);
		jsonToArry(json, "apppagebrand");

		return json;

	}

	static String getAppcollect() {

		String apppageclassurl = appcollectrul_1 + "start_date=" + getYdate()
				+ "&end_date=" + getYdate() + appcollectrul_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("Appcollecturl:" + json);
		jsonToArry(json, "appcollectrul");
		return json;

	}

	/*
	 * 推送个数
	 */
	static String getPushCount() {

		String apppageclassurl = article_url_1 + "start_date=" + getYdate()
				+ "&end_date=" + getYdate() + article_url_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("Appcollecturl:" + json);
		jsonToArry(json, "articlepushcount");
		return json;

	}

	/*
	 * 进入宝典次数
	 */
	static String getArticleClickCount() {
		String apppageclassurl = article_click_url_1 + "start_date="
				+ getYdate() + "&end_date=" + getYdate() + article_click_url_2;
		System.out.println(apppageclassurl);
		String json = HttpUtils.APP(apppageclassurl);
		System.out.println("Appcollecturl:" + json);
		jsonToArry(json, "articleclickcount");
		return json;
	}

	static void ReadF() {

		try {
			// read file content from file

			FileReader reader = new FileReader("data/" + PATH + ".txt");
			BufferedReader br = new BufferedReader(reader);

			String str = null;

			while ((str = br.readLine()) != null) {
				if (str.split("\t")[0].trim().equals("carts")) {
					shopCount.setCarts(Integer.valueOf(str.split("\t")[1]
							.trim()));

				} else if (str.split("\t")[0].trim().equals("orders")) {
					shopCount.setOrders(Integer.valueOf(str.split("\t")[1]
							.trim()));
				} else {
					ProductCount productCount = new ProductCount();
					productCount.setId(str.split("\t")[0].trim());
					productCount.set_name(str.split("\t")[1].trim());
					productCount.set_url(str.split("\t")[2].trim());
					System.err.println(str.split("\t")[3].trim() + "-----");

					productCount.setBuycount(Integer.valueOf(str.split("\t")[3]
							.trim()));
					pro_dbdata.put(productCount.getId(), productCount);
				}

			}
			System.out.println("datasixe:" + pro_dbdata.size());

			br.close();
			reader.close();
		} catch (IOException v) {
			v.printStackTrace();
		}
	}

	static String getProductsJson(String url_1, String url_2) {
		String url = url_1 + "start_date=" + getYdate() + "&end_date="
				+ getYdate() + url_2;
		System.out.println(url);
		String json = HttpUtils.APP(url);
		System.out.println("ProductsJson:" + json);
		jsonToArry(json, "ProductsJson");
		return json;

	}

	// static void MergeProduct(List<ProductCount> productCounts) {
	// for (ProductCount productCount : productCounts) {
	// if(k_v_productCountData.containsKey(productCount.getId())){
	// k_v_productCountData.put(productCount.getId(),k_v_productCountData.get(productCount.getId())+productCount.getPv());
	// }else{
	// k_v_productCountData.put(productCount.getId(),productCount.getPv());
	// }
	//
	// }
	// System.out.println("k_v_productCountDataSize��"+k_v_productCountData.size());
	// Iterator iterator = k_v_productCountData.keySet().iterator();
	// while (iterator.hasNext()) {
	// Object key = iterator.next();
	// System.out.println("key :"+key+":value :"+k_v_productCountData.get(key));
	// }
	//
	// }

	static void jsonToArry(String json, String disting) {

		JSONObject rootJsonObject = null;
		try {
			rootJsonObject = new JSONObject(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Iterator<String> keysIterator = rootJsonObject.keys();
		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			// System.out.println("Jsonkey:" + key);

			if ("result".equals(key)) {
				try {
					String value = rootJsonObject.getString(key);
					if (value.equals("success")) {
						JSONArray urldatas = rootJsonObject
								.getJSONArray("stats");

						for (int i = 0; i < urldatas.length(); i++) {
							JSONObject urldata = urldatas.getJSONObject(i);
							if (disting.equals("apppageproductsurl")) {

								PageHome pageHome = new PageHome();
								pageHome.set_id(0);
								pageHome.set_pv(Integer.valueOf(urldata.get(
										"num").toString()));
								pageHome.setD_id(4);
								pageHome.setDes(IdUtils
										.decryptProductIdFromJNI(urldata.get(
												"label").toString()));
								pageHome.setDataTime(getYdate());
								pageHome.setImpTime(getTdate());

								if (pro_dbdata.containsKey(IdUtils
										.decryptProductIdFromJNI(urldata.get(
												"label").toString()))) {

									pageHome.setNum(pro_dbdata
											.get(IdUtils
													.decryptProductIdFromJNI(urldata
															.get("label")
															.toString()))
											.getBuycount());
									pageHome.set_name(pro_dbdata
											.get(IdUtils
													.decryptProductIdFromJNI(urldata
															.get("label")
															.toString()))
											.get_name());
									pageHome.set_url(pro_dbdata
											.get(IdUtils
													.decryptProductIdFromJNI(urldata
															.get("label")
															.toString()))
											.get_url());
								} else {
									pageHome.setNum(0);
									pageHome.set_url("nulldata");
									pageHome.set_name("nulldata");
								}
								pagedata.add(pageHome);

							} else if (disting.equals("apppageclassurl")) {

								PageHome pageHome = new PageHome();
								pageHome.set_id(0);
								pageHome.set_pv(Integer.valueOf(urldata.get(
										"num").toString()));
								pageHome.setD_id(1);
								pageHome.setNum(0);
								pageHome.setDes(urldata.get("label").toString());
								pageHome.setDataTime(getYdate());
								pageHome.setImpTime(getTdate());
								pagedata.add(pageHome);
							} else if (disting.equals("apppagebrand")) {

								PageHome pageHome = new PageHome();
								pageHome.set_id(0);
								pageHome.set_pv(Integer.valueOf(urldata.get(
										"num").toString()));
								pageHome.setD_id(2);
								pageHome.setNum(0);
								pageHome.setDes(urldata.get("label").toString());
								pageHome.setDataTime(getYdate());
								pageHome.setImpTime(getTdate());
								pagedata.add(pageHome);

							} else if (disting.equals("apppagecamurl")) {

								PageHome pageHome = new PageHome();
								pageHome.set_id(0);
								pageHome.set_pv(Integer.valueOf(urldata.get(
										"num").toString()));
								pageHome.setD_id(3);
								pageHome.setNum(0);
								pageHome.setDes(urldata.get("label").toString());
								pageHome.setDataTime(getYdate());
								pageHome.setImpTime(getTdate());
								pagedata.add(pageHome);

							} else if (disting.equals("appusertodayurl")) {
								if (i == 0) {
									shopCount.setNew_add(Integer
											.valueOf(urldata
													.get("install_data")
													.toString()));
									shopCount.setActivity(Integer
											.valueOf(urldata.get("active_data")
													.toString()));
								}
							} else if (disting.equals("appcollectrul")) {
								shopCount.setCollect(Integer.valueOf(urldata
										.get("count").toString()));

							} else if (disting.equals("ProductsJson")) {
								ProductCount productCount = new ProductCount();
								productCount.setId(IdUtils
										.decryptProductIdFromJNI(urldata.get(
												"label").toString()));
								productCount.setPv(Integer.valueOf(urldata.get(
										"num").toString()));
								if (pro_dbdata.containsKey(IdUtils
										.decryptProductIdFromJNI(urldata.get(
												"label").toString()))) {

									productCount
											.setBuycount(pro_dbdata
													.get(IdUtils
															.decryptProductIdFromJNI(urldata
																	.get("label")
																	.toString()))
													.getBuycount());

									productCount
											.set_name(pro_dbdata
													.get(IdUtils
															.decryptProductIdFromJNI(urldata
																	.get("label")
																	.toString()))
													.get_name());
									productCount
											.set_url(pro_dbdata
													.get(IdUtils
															.decryptProductIdFromJNI(urldata
																	.get("label")
																	.toString()))
													.get_url());
								} else {
									productCount.setBuycount(0);
									productCount.set_name("nulldata");
									productCount.set_url("nulldata");

								}

								if (k_v_productCountData.containsKey(IdUtils
										.decryptProductIdFromJNI(urldata.get(
												"label").toString()))) {
									productCount
											.setPv(Integer.valueOf(urldata.get(
													"num").toString())
													+ (Integer) k_v_productCountData
															.get(IdUtils
																	.decryptProductIdFromJNI(urldata
																			.get("label")
																			.toString()))
															.getPv());

								}
								k_v_productCountData.put(IdUtils
										.decryptProductIdFromJNI(urldata.get(
												"label").toString()),
										productCount);
							} else if (disting.equals("articlepushcount")) {
								// 推送宝典计数
								if (urldata.get("label").equals("Article")) {
									int pushcount = urldata.getInt("num");
									System.out
											.println("pushcount:" + pushcount);
								}

							} else if (disting.equals("articleclickcount")) {

								// 宝典点开的次数
								try {
									int a_id = urldata.getInt("label");
									int a_count = urldata.getInt("num");
									a_clickedsHashMap.put(a_id, a_count);
									System.out.println("article_id:" + a_id
											+ "<->article_count:" + a_count);
									
								} catch (JSONException je) {

								}

							}

						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(-1);
				}

			}
		}

	}

	static String getYdate() {
		if (!("".equals(check.trim()))) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			try {
				dateFormat.parse(check.trim());

			} catch (Exception e) {

				System.out.println("时间格式错误");
				System.exit(-1);
			}

			return check;
		}

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal
				.getTime());
		return yesterday;
	}

	static String getTdate() {

		String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		return today;
	}

	// CUDR

	public static boolean addUserCountinfo() {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = DBMysqlConnect.getInstance().getDBConnection();
			stmt = conn.createStatement();

			StringBuffer sql = new StringBuffer(
					"insert into um_usercount values(");
			sql.append(null + ",");
			sql.append(shopCount.getActivity() + ",");
			sql.append(shopCount.getNew_add() + ",");
			sql.append(shopCount.getOrders() + ",");
			sql.append(shopCount.getCarts() + ",");
			sql.append(shopCount.getCollect() + ",");
			sql.append("'" + getYdate() + "',");
			sql.append("'" + getTdate() + "')");
			System.err.println("sql=" + sql.toString());

			System.out.println("addAction:" + sql.toString());
			int result = stmt.executeUpdate(sql.toString());
			stmt.close();
			conn.close();
			return result > 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean AddPageHomeinfo() {
		Connection conn = null;
		Statement stmt = null;

		try {
			conn = DBMysqlConnect.getInstance().getDBConnection();
			stmt = conn.createStatement();
			conn.setAutoCommit(false);

			for (int i = 0; i < pagedata.size(); i++) {

				StringBuffer sql = new StringBuffer(
						"insert into um_homepage values(");
				sql.append(null + ",'");
				sql.append(pagedata.get(i).get_pv() + "',");
				sql.append("'" + pagedata.get(i).getD_id() + "',");
				sql.append("'" + pagedata.get(i).getDes() + "',");
				sql.append("'" + pagedata.get(i).getNum() + "',");
				sql.append("'" + pagedata.get(i).get_name() + "',");
				sql.append("'" + pagedata.get(i).get_url() + "',");
				sql.append("'" + getYdate() + "',");
				sql.append("'" + getTdate() + "');");
				stmt.executeUpdate(sql.toString());
				System.err.println("sql=" + sql.toString());
			}
			conn.commit();

		} catch (Exception e) {

			e.printStackTrace();
			return false;
		} finally {

			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return true;

	}

	static void GetServerDbInfo() {
		String s_time = " '" + getYdate() + " 00:00:00' and " + " '"
				+ getYdate() + " 23:59:59'";

		AppendToFile appendToFile = new AppendToFile();
		Statement stmt = null;
		Statement stmt1 = null;
		Statement stmt2 = null;

		Connection conn = null;
		appendToFile.Fdel(getYdate());
		try {
			conn = DbServerMysqlConnect.getInstance().getDBConnection();
			stmt = conn.createStatement();
			stmt1 = conn.createStatement();
			stmt2 = conn.createStatement();
			ResultSet rs = stmt
					.executeQuery("select count(distinct device_id) carts from  carts where created_at BETWEEN"
							+ s_time + ";");

			while (rs.next()) {
				appendToFile.readOutFile("carts" + "\t" + rs.getString("carts")
						+ "\n", getYdate());

			}
			stmt.close();
			ResultSet rs1 = stmt1
					.executeQuery("select count(distinct device_id) orders from  orders where created_at BETWEEN"
							+ s_time + " and deleted_at is null;");
			while (rs1.next()) {
				appendToFile.readOutFile(
						"orders" + "\t" + rs1.getString("orders") + "\n",
						getYdate());
				System.out.println(rs1.getString("orders"));
			}
			stmt1.close();
			String sql = "select p.id,p.title,p.detail_link,ifnull(c.cc, 0) from products p left join (SELECT products.id pid,count(line_items.order_id) cc FROM products inner join   line_items on(products.id=line_items.product_id) inner join orders on(line_items.order_id=orders.id) where  (orders.created_at BETWEEN"
					+ s_time
					+ ") and  (products.deleted_at IS NULL) and (products.detail_link is not null) and (orders.deleted_at IS NULL)   group by products.id) c on (p.id=c.pid) where  (p.deleted_at IS NULL) and (p.detail_link is not null);";
			System.out.println(sql);
			ResultSet rs2 = stmt2.executeQuery(sql);
			while (rs2.next()) {

				String pname = rs2.getString(2);
				if (rs2.getString(2).contains("\t")
						|| rs2.getString(2).contains("\n")
						|| rs2.getString(2).contains("\r")) {
					pname = pname.replaceAll("\\s", "");

				}

				appendToFile.readOutFile(rs2.getString(1) + "\t" + pname + "\t"
						+ rs2.getString(3) + "\t" + rs2.getString(4) + "\n",
						getYdate());
			}

			stmt2.close();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {

			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public static boolean AddProductsinfo() {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = DBMysqlConnect.getInstance().getDBConnection();
			stmt = conn.createStatement();
			conn.setAutoCommit(false);

			Iterator iterator = k_v_productCountData.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = iterator.next();
				System.out.println("key :" + key + ":value :"
						+ k_v_productCountData.get(key));

				StringBuffer sql = new StringBuffer(
						"insert into um_products_info values(");
				sql.append(null + ",'");
				sql.append(k_v_productCountData.get(key).getId() + "',");
				sql.append("'" + k_v_productCountData.get(key).getPv() + "',");
				sql.append("'" + k_v_productCountData.get(key).getBuycount()
						+ "',");
				sql.append("'" + k_v_productCountData.get(key).get_name()
						+ "',");
				sql.append("'" + k_v_productCountData.get(key).get_url() + "',");

				sql.append("'" + getYdate() + "',");
				sql.append("'" + getTdate() + "');");
				stmt.executeUpdate(sql.toString());
				System.err.println("sql=" + sql.toString());

			}

			conn.commit();

		} catch (Exception e) {

			e.printStackTrace();
			return false;
		} finally {

			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return true;
	}

	static void delRepeat() {
		Connection conn = null;
		Statement stmt = null;

		try {
			conn = DBMysqlConnect.getInstance().getDBConnection();
			stmt = conn.createStatement();

			conn.setAutoCommit(true);
			System.out.println("start run delete repeatdate");
			String d_pagehome = "delete from um_homepage where _datetime='"
					+ PATH + "';";
			String d_pro_info = "delete from um_products_info where _datetime='"
					+ PATH + "';";
			String d_usercount = "delete from um_usercount where _datetime='"
					+ PATH + "';";

			stmt.executeUpdate(d_pagehome);
			System.out.println(d_pagehome);
			stmt.executeUpdate(d_pro_info);
			System.out.println(d_pro_info);
			stmt.executeUpdate(d_usercount);
			System.out.println(d_usercount);
			stmt.close();
			System.out.println("clear repeatdata is over");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("delete repeatdata error");
			System.exit(-1);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

	static void delAll() {
		Connection conn = null;
		Statement stmt = null;

		try {
			conn = DBMysqlConnect.getInstance().getDBConnection();
			stmt = conn.createStatement();

			conn.setAutoCommit(true);
			System.out.println("start run delete all");
			String d_pagehome = "delete from um_homepage;";
			String d_pro_info = "delete from um_products_info;";
			String d_usercount = "delete from um_usercount;";

			stmt.executeUpdate(d_pagehome);
			System.out.println(d_pagehome);
			stmt.executeUpdate(d_pro_info);
			System.out.println(d_pro_info);
			stmt.executeUpdate(d_usercount);
			System.out.println(d_usercount);
			stmt.close();
			System.out.println("clear all is over");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("delete all error");
			System.exit(-1);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
